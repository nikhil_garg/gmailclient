﻿using FluentAssertions;
using MailClient.Controllers.Helper;
using NUnit.Framework;

namespace Mail.Test.UnitTests.Client.UserMail.Helper
{
    [TestFixture]
    public class UserMailHelperTest
    {
        private IUserMailHelper _userMailHelper;
            
        [SetUp]
        public void SetUp()
        {
            _userMailHelper = new UserMailHelper();
        }

        [Test]
        public void GetMailBox()
        {
            _userMailHelper.GetMailBox("Inbox").ShouldBeEquivalentTo("Inbox");
            _userMailHelper.GetMailBox("Sent").ShouldBeEquivalentTo("[Gmail]/Sent Mail");
        }
    }
}
