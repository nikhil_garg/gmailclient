﻿using System;
using System.Collections.Generic;
using MailClient.Dtos;
using MailClient.Responses.Dtos;

namespace Mail.Test.UnitTests.Client.UserMail.CommonUtils
{
    public class TestOutputData
    {
        public static MailDetails MailMessageResponseTestOutput = new MailDetails
        {
            Id = "100",
            Subject = "Test Subject",
            Body = "",
            Date = new DateTime(2016, 3, 30),
            Seen = true,
            Important = false,
            From = GetMailAddress("from@gmail.com", "From"),
            To = new List<MailAddress>{ GetMailAddress("to@gmail.com", "To") },
            Cc = new List<MailAddress> { GetMailAddress("cc@gmail.com", "Cc") },
            Bcc = new List<MailAddress> { GetMailAddress("bcc@gmail.com", "Bcc") },
            ReplyTo = new List<MailAddress> { GetMailAddress("replyTo@gmail.com", "replyTo") }
        };
        
        private static MailAddress GetMailAddress(string address, string displayName)
        {
            return new MailAddress
            {
                Address = address,
                DisplayName = displayName
            };
        } 
        
    }
}
