﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using MailClient.Requests.Dtos;
using MailDomain;
using MailDomain.Configurations.Imap;
using MailDomain.Configurations.Smtp;
using MailDomain.Dtos;
using MailDomain.MailReceiver.Dto;
using MailDomain.MailRemover.Dto;
using MailDomain.MailSender.Dto;

namespace Mail.Test.UnitTests.Client.UserMail.CommonUtils
{
    public class TestInputData
    {
        public static UserProfile UserProfileTestInput = new UserProfile
        {
            UserId = 1,
            UserName = "abc@gmail.com",
            Password = "abcPassword"
        };

        public static MailMessageDto GetMailMessageTestInput()
        {
            var mailMessage = new MailMessageDto
            {
                Id = "100",
                Subject = "Test Subject",
                Body = "",
                Date = new DateTime(2016, 3, 30),
                Seen = true,
                Important = false,
                From = GetMailAddress("from@gmail.com", "From")
            };
            mailMessage.To.Add(GetMailAddress("to@gmail.com", "To"));
            mailMessage.CC.Add(GetMailAddress("cc@gmail.com", "Cc"));
            mailMessage.Bcc.Add(GetMailAddress("bcc@gmail.com", "Bcc"));
            mailMessage.ReplyToList.Add(GetMailAddress("replyTo@gmail.com", "replyTo"));

            return mailMessage;
        }

        public static MailReceiverDto GetMailReceiverRequestTestInput(IImapConfiguration imapConfiguration)
        {
            return new MailReceiverDto
            {
                Id = "100",
                MailBox = "Inbox",
                StartIndex = 0,
                EndIndex = 10,
                Imap = imapConfiguration,
                RequireDetails = false
            };
        }

        public static MailRemoverDto GetMailRemoverRequestTestInput(IImapConfiguration imapConfiguration)
        {
            return new MailRemoverDto
            {
                Ids =  new List<string> { "100" },
                MailBox = "Inbox",
                Imap = imapConfiguration,
            };
        }

        public static DeleteMailRequest GetDeleteMailRequestTestInput()
        {
            return new DeleteMailRequest
            {
                Ids = new List<string> { "100" }
            };
        }

        public static MailSenderDto GetMailSenderRequestTestInput(ISmtpConfiguration smtpConfiguration)
        {
            return new MailSenderDto
            {
                MailMessage = GetMailMessageTestInput(),
                Smtp = smtpConfiguration
            };
        }

        public static SendMailRequest GetSendMailRequestTestInput(string userName)
        {
            return new SendMailRequest
            {
                Subject = "Test Subject",
                Body = "Test Body",
                To = new List<MailClient.Dtos.MailAddress>{ new MailClient.Dtos.MailAddress { Address = "to@gmail.com", DisplayName = "To" } },
                Cc = new List<MailClient.Dtos.MailAddress>{ new MailClient.Dtos.MailAddress { Address = "cc@gmail.com", DisplayName = "Cc" } },
                Bcc = new List<MailClient.Dtos.MailAddress>{ new MailClient.Dtos.MailAddress { Address = "bcc@gmail.com", DisplayName = "Bcc" } },
                ReplyTo = new List<MailClient.Dtos.MailAddress>{ new MailClient.Dtos.MailAddress { Address = "replyTo@gmail.com", DisplayName = "replyTo" } }
            };
        }

        private static MailAddress GetMailAddress(string address, string displayName)
        {
            return new MailAddress(address, displayName);
        }
    }
}
