﻿using FluentAssertions;
using Mail.Test.UnitTests.Client.UserMail.CommonUtils;
using MailClient.Controllers.Api;
using MailClient.Controllers.Helper;
using MailClient.Responses.Dtos;
using MailDomain;
using MailDomain.Configurations.Imap;
using MailDomain.Configurations.Smtp;
using MailDomain.Dtos;
using MailDomain.MailReceiver.Receiver;
using MailDomain.MailRemover.Remover;
using MailDomain.MailSender.Sender;
using NSubstitute;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Principal;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Hosting;

namespace Mail.Test.UnitTests.Client.UserMail.Api
{
    [TestFixture]
    public class SendApiTest
    {
        private UserMailController _userMailController;
        private UserProfile _userProfile;
        private IMailSender _mailSender;
        private IUserMailHelper _userMailHelper;
        private ISmtpConfiguration _smtpConfiguration;

        [SetUp]
        public void SetUp()
        {
            _userProfile = TestInputData.UserProfileTestInput;
            InitializeUserMailController();
        }

        [Test]
        public void SendMailTest_Pass()
        {
            Thread.CurrentPrincipal = new GenericPrincipal(new GenericIdentity(_userProfile.UserName), Enumerable.Empty<string>().ToArray());

            _mailSender
                .SendMail(TestInputData.GetMailSenderRequestTestInput(_smtpConfiguration))
                .ReturnsForAnyArgs(true);
            _userMailHelper
                .GetUser(_userProfile.UserName)
                .Returns(_userProfile);

            var httpResponseMessage = _userMailController.Send(_userProfile.UserName, TestInputData.GetSendMailRequestTestInput(_userProfile.UserName));

            httpResponseMessage.EnsureSuccessStatusCode();
        }

        private void InitializeUserMailController()
        {
            var mailRemover = Substitute.For<IMailRemover>();
            var mailReceiver = Substitute.For<IMailReceiver>();
            var imapConfiguration = Substitute.For<IImapConfiguration>();
            _smtpConfiguration = Substitute.For<ISmtpConfiguration>();
            _userMailHelper = Substitute.For<IUserMailHelper>();
            _mailSender = Substitute.For<IMailSender>();
            var httpRequestMessage = new HttpRequestMessage();
            httpRequestMessage.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());

            _userMailController = new UserMailController(_smtpConfiguration, imapConfiguration, mailReceiver, mailRemover, _mailSender, _userMailHelper)
            {
                Request = httpRequestMessage
            };
        }
    }
}
