﻿using FluentAssertions;
using Mail.Test.UnitTests.Client.UserMail.CommonUtils;
using MailClient.Controllers.Api;
using MailClient.Controllers.Helper;
using MailClient.Responses.Dtos;
using MailDomain;
using MailDomain.Configurations.Imap;
using MailDomain.Configurations.Smtp;
using MailDomain.Dtos;
using MailDomain.MailReceiver.Receiver;
using MailDomain.MailRemover.Remover;
using MailDomain.MailSender.Sender;
using NSubstitute;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Principal;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Hosting;

namespace Mail.Test.UnitTests.Client.UserMail.Api
{
    [TestFixture]
    public class RetrieveApiTest
    {
        private UserMailController _userMailController;
        private UserProfile _userProfile;
        private IMailReceiver _mailReceiver;
        private IUserMailHelper _userMailHelper;
        private IImapConfiguration _imapConfiguration;

        [SetUp]
        public void SetUp()
        {
            _userProfile = TestInputData.UserProfileTestInput;
            InitializeUserMailController();
        }

        [Test]
        public void RetrieveMailTest_Pass()
        {
            var expectedRetrieveMailResponse = new MailMessageResponse { data = new List<MailDetails> { TestOutputData.MailMessageResponseTestOutput } };
            Thread.CurrentPrincipal = new GenericPrincipal(new GenericIdentity(_userProfile.UserName), Enumerable.Empty<string>().ToArray());
            const string mailBox = "Inbox";

            _mailReceiver
                .GetMails(TestInputData.GetMailReceiverRequestTestInput(_imapConfiguration))
                .ReturnsForAnyArgs(new List<MailMessageDto> {TestInputData.GetMailMessageTestInput()});
            _userMailHelper
                .GetUser(_userProfile.UserName)
                .Returns(_userProfile);
            _userMailHelper
                .GetMailBox(mailBox)
                .Returns(mailBox);

            var httpResponseMessage = _userMailController.Retrieve(_userProfile.UserName, mailBox, 0, 10);

            httpResponseMessage.EnsureSuccessStatusCode();

            MailMessageResponse actualRetrieveMailResponse;
            httpResponseMessage.TryGetContentValue(out actualRetrieveMailResponse);
            actualRetrieveMailResponse.ShouldBeEquivalentTo(expectedRetrieveMailResponse);
        }

        private void InitializeUserMailController()
        {
            var smtpConfiguration = Substitute.For<ISmtpConfiguration>();
            var mailRemover = Substitute.For<IMailRemover>();
            var mailSender = Substitute.For<IMailSender>();
            _userMailHelper = Substitute.For<IUserMailHelper>();
            _imapConfiguration = Substitute.For<IImapConfiguration>();
            _mailReceiver = Substitute.For<IMailReceiver>();
            var httpRequestMessage = new HttpRequestMessage();
            httpRequestMessage.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());

            _userMailController = new UserMailController(smtpConfiguration, _imapConfiguration, _mailReceiver, mailRemover, mailSender, _userMailHelper)
            {
                Request = httpRequestMessage
            };
        }
    }
}
