﻿using Mail.Test.UnitTests.Client.UserMail.CommonUtils;
using MailClient.Controllers.Api;
using MailClient.Controllers.Helper;
using MailDomain;
using MailDomain.Configurations.Imap;
using MailDomain.Configurations.Smtp;
using MailDomain.MailReceiver.Receiver;
using MailDomain.MailRemover.Remover;
using MailDomain.MailSender.Sender;
using NSubstitute;
using NUnit.Framework;
using System.Linq;
using System.Net.Http;
using System.Security.Principal;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Hosting;

namespace Mail.Test.UnitTests.Client.UserMail.Api
{
    [TestFixture]
    public class DeleteApiTest
    {
        private UserMailController _userMailController;
        private UserProfile _userProfile;
        private IMailRemover _mailRemover;
        private IUserMailHelper _userMailHelper;
        private IImapConfiguration _imapConfiguration;

        [SetUp]
        public void SetUp()
        {
            _userProfile = TestInputData.UserProfileTestInput;
            InitializeUserMailController();
        }

        [Test]
        public void RetrieveMailTest_Pass()
        {
            Thread.CurrentPrincipal = new GenericPrincipal(new GenericIdentity(_userProfile.UserName), Enumerable.Empty<string>().ToArray());
            const string mailBox = "Inbox";

            _mailRemover
                .RemoveMail(TestInputData.GetMailRemoverRequestTestInput(_imapConfiguration))
                .ReturnsForAnyArgs(true);
            _userMailHelper
                .GetUser(_userProfile.UserName)
                .Returns(_userProfile);
            _userMailHelper
                .GetMailBox(mailBox)
                .Returns(mailBox);

            var httpResponseMessage = _userMailController.Delete(_userProfile.UserName, mailBox, TestInputData.GetDeleteMailRequestTestInput());
            
            httpResponseMessage.EnsureSuccessStatusCode();
        }

        private void InitializeUserMailController()
        {
            var smtpConfiguration = Substitute.For<ISmtpConfiguration>();
            var mailReceiver = Substitute.For<IMailReceiver>();
            var mailSender = Substitute.For<IMailSender>();
            _userMailHelper = Substitute.For<IUserMailHelper>();
            _imapConfiguration = Substitute.For<IImapConfiguration>();
            _mailRemover = Substitute.For<IMailRemover>();
            var httpRequestMessage = new HttpRequestMessage();
            httpRequestMessage.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());

            _userMailController = new UserMailController(smtpConfiguration, _imapConfiguration, mailReceiver, _mailRemover, mailSender, _userMailHelper)
            {
                Request = httpRequestMessage
            };
        }
    }
}
