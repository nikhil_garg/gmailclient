﻿using FluentAssertions;
using MailClient.Controllers.Api;
using MailClient.Controllers.Helper;
using MailDomain.Configurations.Imap;
using MailDomain.Configurations.Smtp;
using MailDomain.MailReceiver.Receiver;
using MailDomain.MailRemover.Remover;
using MailDomain.MailSender.Sender;
using NSubstitute;
using NUnit.Framework;
using System.Reflection;
using System.Web.Http;

namespace Mail.Test.UnitTests.Client.UserMail.Attribute
{
    [TestFixture]
    public class ApiAuthorizeTest
    {
        private UserMailController _userMailController;
        private MethodInfo _retrieveMethodInfo;
        private MethodInfo _mailDetailsMethodInfo;
        private MethodInfo _countMethodInfo;
        private MethodInfo _sendMethodInfo;
        private MethodInfo _deleteMethodInfo;

        [SetUp]
        public void SetUp()
        {
            InitializeUserMailController();
            _retrieveMethodInfo = _userMailController.GetType().GetMethod("Retrieve");
            _mailDetailsMethodInfo = _userMailController.GetType().GetMethod("Details");
            _countMethodInfo = _userMailController.GetType().GetMethod("Count");
            _sendMethodInfo = _userMailController.GetType().GetMethod("Send");
            _deleteMethodInfo = _userMailController.GetType().GetMethod("Delete");
        }

        [Test]
        public void UserMailController_Decorated_With_AuthorizeAttribute()
        {
            _userMailController.GetType().IsDefined(typeof(AuthorizeAttribute), true).Should().BeTrue();
        }

        [Test]
        public void Retrieve_Decorated_With_HttpGetAttribute()
        {
            _retrieveMethodInfo.IsDefined(typeof(HttpGetAttribute), true).Should().BeTrue();
        }

        [Test]
        public void MailDetails_Decorated_With_HttpGetAttribute()
        {
            _mailDetailsMethodInfo.IsDefined(typeof(HttpGetAttribute), true).Should().BeTrue();
        }

        [Test]
        public void Count_Decorated_With_HttpGetAttribute()
        {
            _countMethodInfo.IsDefined(typeof(HttpGetAttribute), true).Should().BeTrue();
        }

        [Test]
        public void Send_Decorated_With_HttpPostAttribute()
        {
            _sendMethodInfo.IsDefined(typeof(HttpPostAttribute), true).Should().BeTrue();
        }

        [Test]
        public void Delete_Decorated_With_HttpDeleteAttribute()
        {
            _deleteMethodInfo.IsDefined(typeof(HttpDeleteAttribute), true).Should().BeTrue();
        }

        private void InitializeUserMailController()
        {
            var smtpConfiguration = Substitute.For<ISmtpConfiguration>();
            var imapConfiguration = Substitute.For<IImapConfiguration>();
            var mailReceiver = Substitute.For<IMailReceiver>();
            var mailRemover = Substitute.For<IMailRemover>();
            var mailSender = Substitute.For<IMailSender>();
            var userMailHelper = Substitute.For<IUserMailHelper>();

            _userMailController = new UserMailController(smtpConfiguration, imapConfiguration, mailReceiver, mailRemover, mailSender, userMailHelper);
        }
    }
}
