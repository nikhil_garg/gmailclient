﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MailDomain;

namespace Mail.Test.UnitTests.Repository
{
    public class MockUserProfileRepository : MockRepository<UserProfile>, IRepository<UserProfile>
    {
        public bool Delete(UserProfile entity)
        {
            return Delete(entity.UserId);
        }

        public void Add(UserProfile obj)
        {
            Add(obj.UserId, obj);
        }
    }
}
