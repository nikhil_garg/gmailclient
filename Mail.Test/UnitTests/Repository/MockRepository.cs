﻿using MailDomain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mail.Test.UnitTests.Repository
{
    public abstract class MockRepository<T>
    {
        private readonly IDictionary<Type, IDictionary<object, T>> _context = new Dictionary<Type, IDictionary<object, T>>();

        public IEnumerable<T> GetAll()
        {
            var entityTable = GetEntityTable(typeof(T));
            return entityTable.Select(kvp => kvp.Value);
        }

        public IEnumerable<T> Get(Func<T, bool> predicate)
        {
            var entityTable = GetEntityTable(typeof(T));
            return entityTable.Select(kvp => kvp.Value).Where(predicate);
        }

        public void Add(object id, T entity)
        {
            var entityTable = GetEntityTable(entity.GetType());
            if (entityTable.ContainsKey(entity))
            {
                entityTable[id] = entity;
            }
            else
            {
                entityTable.Add(id, entity);
            }
        }

        public bool Delete(int id)
        {
            var deleteStatus = false;
            var entityTable = GetEntityTable(typeof(T));
            if (entityTable.ContainsKey(id))
            {
                var obj = entityTable[id];
                entityTable.Remove(obj);
                deleteStatus = true;
            }

            return deleteStatus;
        }

        private IDictionary<object, T> GetEntityTable(Type type)
        {
            IDictionary<object, T> entityTable;
            var hasTable = _context.TryGetValue(type, out entityTable);
            if (!hasTable)
            {
                entityTable = new Dictionary<object, T>();
                _context.Add(type, entityTable);
            }
                
            return entityTable;
        }
    }
}
