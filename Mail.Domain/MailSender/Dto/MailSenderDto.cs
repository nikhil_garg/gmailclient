﻿using MailDomain.Configurations.Smtp;
using MailMessage = System.Net.Mail.MailMessage;

namespace MailDomain.MailSender.Dto
{
    public class MailSenderDto
    {
        public MailMessage MailMessage { get; set; }
        public ISmtpConfiguration Smtp { get; set; }
    }
}
