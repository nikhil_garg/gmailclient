﻿using System;
using System.Net.Mail;
using log4net;
using MailDomain.MailSender.Dto;

namespace MailDomain.MailSender.Sender
{
    public class MailSender : IMailSender
    {
        private readonly ILog _log;

        public MailSender()
        {
            _log = LogManager.GetLogger(typeof(MailSender));
        }

        public bool SendMail(MailSenderDto mailSenderDto)
        {
            try
            {
                using (var smtpClient = new SmtpClient())
                {
                    smtpClient.Host = mailSenderDto.Smtp.Server;
                    smtpClient.Port = mailSenderDto.Smtp.Port;
                    smtpClient.Credentials = new System.Net.NetworkCredential(mailSenderDto.Smtp.Username,
                        mailSenderDto.Smtp.Password);
                    smtpClient.EnableSsl = mailSenderDto.Smtp.Ssl;
                    smtpClient.UseDefaultCredentials = false;
                    smtpClient.Send(mailSenderDto.MailMessage);
                }

                return true;
            }
            catch (Exception ex)
            {
                var logMailException =
                    string.Format(
                        "Exception sending email : To : {0}, From : {1}, Subject : {2}, Body : {3}, Time : {4}, Exception : {5}",
                        mailSenderDto.MailMessage.To, mailSenderDto.MailMessage.From,
                        mailSenderDto.MailMessage.Subject, mailSenderDto.MailMessage.Body, DateTime.Now, ex);
                _log.Error(logMailException);

                throw;
            }
        }
    }
}
