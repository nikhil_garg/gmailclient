﻿using System.Net.Mail;
using MailDomain.MailSender.Dto;

namespace MailDomain.MailSender.Sender
{
    public interface IMailSender
    {
        bool SendMail(MailSenderDto mailSenderDto);
    }
}
