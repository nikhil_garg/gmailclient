﻿using MailDomain.MailRemover.Dto;

namespace MailDomain.MailRemover.Remover
{
    public interface IMailRemover
    {
        bool RemoveMail(MailRemoverDto mailRemoverDto);
    }
}
