﻿using AE.Net.Mail;
using log4net;
using MailDomain.MailRemover.Dto;
using System;

namespace MailDomain.MailRemover.Remover
{
    public class MailRemover : IMailRemover
    {
        private readonly ILog _log;

        public MailRemover()
        {
            _log = LogManager.GetLogger(typeof(MailRemover));
        }

        public bool RemoveMail(MailRemoverDto mailRemoverDto)
        {
            ImapClient imapClient = null;
            try
            {
                imapClient = new ImapClient(mailRemoverDto.Imap.Server, mailRemoverDto.Imap.Username,
                    mailRemoverDto.Imap.Password, mailRemoverDto.Imap.AuthMode, mailRemoverDto.Imap.Port,
                    mailRemoverDto.Imap.Ssl);
                imapClient.SelectMailbox(mailRemoverDto.MailBox);
                foreach (var id in mailRemoverDto.Ids)
                {
                    imapClient.DeleteMessage(id);
                }

                return true;
            }
            catch (Exception ex)
            {
                var logMailException =
                    string.Format(
                        "Exception while deleting email : Ids : {0}, Time : {1}, Exception : {2}",
                        string.Join(",", mailRemoverDto.Ids), DateTime.Now, ex);
                _log.Error(logMailException);

                throw;
            }
            finally
            {
                if (imapClient != null)
                {
                    imapClient.Dispose();
                }
            }
        }
    }
}
