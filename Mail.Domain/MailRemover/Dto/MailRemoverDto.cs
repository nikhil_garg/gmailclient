﻿using MailDomain.Configurations.Imap;
using System.Collections.Generic;

namespace MailDomain.MailRemover.Dto
{
    public class MailRemoverDto
    {
        public string MailBox { get; set; }
        public IEnumerable<string> Ids { get; set; }
        public IImapConfiguration Imap { get; set; }
    }
}
