﻿using System.Collections.Generic;
using System.Net.Mail;

namespace MailDomain.MailRemover.Extension
{
    public static class MailRemoverExtension
    {
        public static IEnumerable<AE.Net.Mail.MailMessage> ToAeMailMessage(this IEnumerable<MailMessage> mailMessages)
        {
            var aeMailMessages = new List<AE.Net.Mail.MailMessage>();

            foreach (var mailMessage in mailMessages)
            {
                var aeMailMessage = new AE.Net.Mail.MailMessage
                {
                    Subject = mailMessage.Subject,
                    Body = mailMessage.Body,
                    From = mailMessage.From,
                };

                foreach (var to in mailMessage.To)
                {
                    aeMailMessage.To.Add(to);
                }

                foreach (var cc in mailMessage.CC)
                {
                    aeMailMessage.Cc.Add(cc);
                }

                foreach (var bcc in mailMessage.Bcc)
                {
                    aeMailMessage.Bcc.Add(bcc);
                }

                foreach (var replyTo in mailMessage.ReplyToList)
                {
                    aeMailMessage.ReplyTo.Add(replyTo);
                }

                aeMailMessages.Add(aeMailMessage);
            }

            return aeMailMessages;
        }
    }
}
