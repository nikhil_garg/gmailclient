﻿using System.Configuration;

namespace MailDomain.Configurations.Smtp
{
    public class SmtpConfiguration : ISmtpConfiguration
    {
        private const int DefaultPort = 587;

        public string Server
        {
            get
            {
                return ConfigurationManager.AppSettings["SmtpServer"];
            }
        }

        public int Port
        {
            get
            {
                int port;
                if (int.TryParse(ConfigurationManager.AppSettings["SmtpPort"], out port))
                    return port;
                return DefaultPort;
            }
        }

        public string Username { get; set; }

        public string Password { get; set; }        

        public bool Ssl
        {
            get
            {
                bool ssl;
                if (bool.TryParse(ConfigurationManager.AppSettings["SmtpSsl"], out ssl))
                    return ssl;
                return false;
            }
        }
    }
}
