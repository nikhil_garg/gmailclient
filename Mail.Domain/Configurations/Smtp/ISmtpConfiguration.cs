﻿
namespace MailDomain.Configurations.Smtp
{
    public interface ISmtpConfiguration
    {
        string Server { get; }
        int Port { get; }
        string Username { get; set; }
        string Password { get; set; }
        bool Ssl { get; }
    }
}
