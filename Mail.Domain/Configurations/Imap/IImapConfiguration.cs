﻿using AE.Net.Mail;

namespace MailDomain.Configurations.Imap
{
    public interface IImapConfiguration
    {
        string Server { get; }
        int Port { get; }
        AuthMethods AuthMode { get; }
        bool Ssl { get; }
        string Username { get; set; }
        string Password { get; set; }
    }
}
