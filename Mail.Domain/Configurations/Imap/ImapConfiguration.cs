﻿using System.Configuration;
using AE.Net.Mail;

namespace MailDomain.Configurations.Imap
{
    public class ImapConfiguration : IImapConfiguration
    {
        private const int DefaultPort = 993;

        public string Server
        {
            get { return ConfigurationManager.AppSettings["ImapServer"]; }
        }

        public int Port
        {
            get
            {
                int port;
                if (int.TryParse(ConfigurationManager.AppSettings["ImapPort"], out port))
                    return port;
                return DefaultPort;
            }
        }

        public AuthMethods AuthMode
        {
            get
            {
                return AuthMethods.Login;
            }
        }

        public bool Ssl
        {
            get
            {
                bool ssl;
                if (bool.TryParse(ConfigurationManager.AppSettings["ImapSsl"], out ssl))
                    return ssl;
                return false;
            }
        }

        public string Username { get; set; }

        public string Password { get; set; }        
    }
}
