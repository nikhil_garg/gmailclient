﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace MailDomain
{
    public interface IRepository<T> where T : class
    {
        IEnumerable<T> GetAll();
        IEnumerable<T> Get(Func<T, bool> predicate);
        void Add(T obj);
        bool Delete(T obj);
        bool Delete(int id);
    }
}
