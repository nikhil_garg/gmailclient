﻿using Ninject.Modules;
using Ninject.Extensions.Conventions;

namespace MailDomain.Ninject
{
    public class NinjectRegistry : NinjectModule
    {
        public override void Load()
        {
            System.Diagnostics.Debug.WriteLine(string.Format("### in {0}.Load", typeof(NinjectRegistry).FullName));

            Kernel.Bind(ctx => ctx
                .FromThisAssembly()
                .SelectAllTypes()
                .BindAllInterfaces());
        }
    }
}
