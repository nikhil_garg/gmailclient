﻿using System.Linq;
using AE.Net.Mail;
using System.Collections.Generic;
using MailDomain.Dtos;

namespace MailDomain.MailReceiver.Extension
{
    public static class MailReceiverExtension
    {
        public static IEnumerable<MailMessageDto> ToMailMessage(this IEnumerable<AE.Net.Mail.MailMessage> aeMailMessages, int count = 0)
        {
            var mailMessages = new List<MailMessageDto>();

            foreach (var aeMailMessage in aeMailMessages)
            {
                var mailMessage = new MailMessageDto
                {
                    Id = aeMailMessage.Uid,
                    Subject = aeMailMessage.Subject,
                    Body = aeMailMessage.AlternateViews.Any() ? aeMailMessage.AlternateViews.GetHtmlView().Body : "",
                    From = aeMailMessage.From,
                    Date = aeMailMessage.Date,
                    Important = aeMailMessage.Importance == MailPriority.High,
                    Seen = aeMailMessage.Flags == Flags.Seen,
                    Count = count
                };
                
                foreach (var to in aeMailMessage.To)
                {
                    mailMessage.To.Add(to);
                }

                foreach (var cc in aeMailMessage.Cc)
                {
                    mailMessage.CC.Add(cc);
                }

                foreach (var bcc in aeMailMessage.Bcc)
                {
                    mailMessage.Bcc.Add(bcc);
                }
                
                mailMessages.Add(mailMessage);
            }
            return mailMessages;
        }
    }
}
