﻿using System;
using AE.Net.Mail;
using log4net;
using MailDomain.Dtos;
using MailDomain.MailReceiver.Dto;
using MailDomain.MailReceiver.Extension;
using System.Collections.Generic;
using System.Linq;

namespace MailDomain.MailReceiver.Receiver
{
    public class MailReceiver : IMailReceiver
    {
        private readonly ILog _log;

        public MailReceiver()
        {
            _log = LogManager.GetLogger(typeof(MailReceiver));
        }

        public IEnumerable<MailMessageDto> GetMails(MailReceiverDto mailReceiverDto)
        {
            ImapClient imapClient = null;
            try
            {
                imapClient = GetImapClient(mailReceiverDto);
                imapClient.SelectMailbox(mailReceiverDto.MailBox);
                var count = GetMailCount(mailReceiverDto);
                var startIndex = GetStartIndex(mailReceiverDto, count);
                var endIndex = count - 1 - mailReceiverDto.StartIndex;
                return
                    imapClient.GetMessages(startIndex, endIndex, !mailReceiverDto.RequireDetails)
                        .ToMailMessage(count)
                        .ToList();
            }
            catch (Exception ex)
            {
                LogError(string.Format(
                    "Exception while fetching the emails : Username : {0}, Time : {1}, Exception : {2}",
                    mailReceiverDto.Imap.Username, DateTime.Now, ex));

                throw;
            }
            finally
            {
                Dispose(imapClient);
            }
        }

        public MailMessageDto GetMailDetails(MailReceiverDto mailReceiverDto)
        {
            ImapClient imapClient = null;
            try
            {
                imapClient = GetImapClient(mailReceiverDto);
                imapClient.SelectMailbox(mailReceiverDto.MailBox);
                var aeMailMessaages = new List<MailMessage>
                {
                    imapClient.GetMessage(mailReceiverDto.Id, !mailReceiverDto.RequireDetails)
                };
                return aeMailMessaages.Any() ?  aeMailMessaages.ToMailMessage().First() : new MailMessageDto();
            }
            catch (Exception ex)
            {
                LogError(string.Format(
                    "Exception while fetching the email details : Username : {0}, Time : {1}, Exception : {2}",
                    mailReceiverDto.Imap.Username, DateTime.Now, ex));

                throw;
            }
            finally
            {
                Dispose(imapClient);
            }
        }

        public int GetMailCount(MailReceiverDto mailReceiverDto)
        {
            ImapClient imapClient = null;
            try
            {
                imapClient = GetImapClient(mailReceiverDto);
                return imapClient.GetMessageCount(mailReceiverDto.MailBox);
            }
            catch (Exception ex)
            {
                LogError(string.Format(
                    "Exception while fetching the mailbox count : Username : {0}, Time : {1}, Exception : {2}",
                    mailReceiverDto.Imap.Username, DateTime.Now, ex));

                throw;
            }
            finally
            {
                Dispose(imapClient);
            }
        }

        private ImapClient GetImapClient(MailReceiverDto mailReceiverDto)
        {
            return new ImapClient(mailReceiverDto.Imap.Server, mailReceiverDto.Imap.Username,
                    mailReceiverDto.Imap.Password, mailReceiverDto.Imap.AuthMode, mailReceiverDto.Imap.Port,
                    mailReceiverDto.Imap.Ssl);
        }

        private void LogError(string message)
        {   
            _log.Error(message);
        }

        private void Dispose(ImapClient imapClient)
        {
            if (imapClient != null)
            {
                imapClient.Dispose();
            }
        }

        private int GetStartIndex(MailReceiverDto mailReceiverDto, int count)
        {
            var startndex = count - 1 - mailReceiverDto.EndIndex < 0
                ? 0
                : count - 1 - mailReceiverDto.EndIndex;

            return startndex;
        }
    }
}
