﻿using MailDomain.Dtos;
using MailDomain.MailReceiver.Dto;
using System.Collections.Generic;

namespace MailDomain.MailReceiver.Receiver
{
    public interface IMailReceiver
    {
        IEnumerable<MailMessageDto> GetMails(MailReceiverDto mailReceiverDto);
        MailMessageDto GetMailDetails(MailReceiverDto mailReceiverDto);
        int GetMailCount(MailReceiverDto mailReceiverDto);
    }
}
