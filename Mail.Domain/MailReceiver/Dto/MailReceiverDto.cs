﻿using MailDomain.Configurations.Imap;

namespace MailDomain.MailReceiver.Dto
{
    public class MailReceiverDto
    {
        public string Id { get; set; }
        public int StartIndex { get; set; }
        public int EndIndex { get; set; }
        public int Length { get; set; }
        public string MailBox { get; set; }
        public bool RequireDetails { get; set; }
        public IImapConfiguration Imap { get; set; }
    }
}
