﻿
using System;
using System.Net.Mail;

namespace MailDomain.Dtos
{
    public class MailMessageDto : MailMessage
    {
        public int Count { get; set; }
        public string Id { get; set; }
        public DateTime Date { get; set; }
        public bool Seen { get; set; }
        public bool Important { get; set; }
    }
}
