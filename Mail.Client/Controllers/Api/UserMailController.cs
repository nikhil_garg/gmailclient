﻿using log4net;
using MailClient.Controllers.Helper;
using MailClient.Requests.Dtos;
using MailClient.Requests.Extensions;
using MailClient.Responses.Extensions;
using MailDomain.Configurations.Imap;
using MailDomain.Configurations.Smtp;
using MailDomain.MailReceiver.Receiver;
using MailDomain.MailRemover.Remover;
using MailDomain.MailSender.Sender;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MailClient.Controllers.Api
{
    [Authorize]
    public class UserMailController : ApiController
    {
        private readonly ILog _log;
        private readonly ISmtpConfiguration _smtpConfiguration;
        private readonly IImapConfiguration _imapConfiguration;
        private readonly IMailReceiver _mailReceiver;
        private readonly IMailRemover _mailRemover;
        private readonly IMailSender _mailSender;
        private readonly IUserMailHelper _userMailHelper;

        public UserMailController(ISmtpConfiguration smtpConfiguration, IImapConfiguration imapConfiguration, IMailReceiver mailReceiver,
                                  IMailRemover mailRemover, IMailSender mailSender, IUserMailHelper userMailHelper)
        {
            _log = LogManager.GetLogger(typeof(UserMailController));
            _smtpConfiguration = smtpConfiguration;
            _imapConfiguration = imapConfiguration;
            _mailReceiver = mailReceiver;
            _mailRemover = mailRemover;
            _mailSender = mailSender;
            _userMailHelper = userMailHelper;
        }

        // GET: /Users/{userName}/{mailBox}/Mail
        [HttpGet]
        public HttpResponseMessage Retrieve(string userName, string mailBox, int start, int length)
        {
            try
            {
                var user = _userMailHelper.GetUser(userName);

                return Request.CreateResponse(HttpStatusCode.OK, 
                    _mailReceiver.GetMails(UserMailRequestExtension.GetRetrieveMailRequest(userName, _userMailHelper.GetMailBox(mailBox), start, start + length - 1, length)
                                                .ToMailReceiverDto(user.Password, false, _imapConfiguration))
                                                .ToMailMessageResponseList());
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        // GET: /Users/{userName}/{mailBox}/Count
        [HttpGet]
        public HttpResponseMessage Count(string userName, string mailBox)
        {
            try
            {
                var user = _userMailHelper.GetUser(userName);

                return Request.CreateResponse(HttpStatusCode.OK,
                    _mailReceiver.GetMailCount(UserMailRequestExtension.GetRetrieveMailRequest(userName, _userMailHelper.GetMailBox(mailBox))
                                                    .ToMailReceiverDto(user.Password, false, _imapConfiguration)));
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        // GET: /Users/{userName}/{mailBox}/Mail/{messageId}
        [HttpGet]
        public HttpResponseMessage Details(string userName, string mailBox, string messageId)
        {
            try
            {
                var user = _userMailHelper.GetUser(userName);

                return Request.CreateResponse(HttpStatusCode.OK,
                    _mailReceiver.GetMailDetails(UserMailRequestExtension.GetMailDetailsRequest(userName, _userMailHelper.GetMailBox(mailBox), messageId)
                                                    .ToMailDetailsDto(user.Password, true, _imapConfiguration))
                                                    .ToMailMessageResponse());
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        // POST: /Users/{userName}/Mail/Send
        [HttpPost]
        public HttpResponseMessage Send(string userName, [FromBody] SendMailRequest sendMailRequest)
        {
            try
            {
                var user = _userMailHelper.GetUser(userName);

                var sendStatus = _mailSender.SendMail(sendMailRequest.ToMailSenderDto(userName, user.Password, _smtpConfiguration));

                if (sendStatus)
                {
                    return Request.CreateResponse(HttpStatusCode.OK);
                }

                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        // DELETE: /Users/{userName}/{mailBox}/Delete/
        [HttpDelete]
        public HttpResponseMessage Delete(string userName, string mailBox, DeleteMailRequest deleteMailRequest)
        {
            try
            {
                var user = _userMailHelper.GetUser(userName);

                var removeStatus = _mailRemover.RemoveMail(deleteMailRequest.ToMailRemoverDto(userName, user.Password, mailBox, _imapConfiguration));

                if (removeStatus)
                {
                    return Request.CreateResponse(HttpStatusCode.OK);
                }

                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        private HttpResponseMessage HandleException(Exception ex)
        {
            _log.Error(ex);
            return Request.CreateResponse(HttpStatusCode.BadRequest, new { error = ex.Message });
        }
    }
}
