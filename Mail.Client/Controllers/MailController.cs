﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MailClient.Models;

namespace MailClient.Controllers
{
    [Authorize]
    public class MailController : Controller
    {
        //
        // GET: /Mail/

        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /Mail/SentMail

        public ActionResult SentMail()
        {
            return View();
        }

        //
        // GET: /Mail/Details?mailBox=Inbox&id=6

        public ActionResult Details(string mailBox, int id)
        {
            var mailModel = new MailModel
            {
                MailId = id,
                MailBox = mailBox
            };

            return View(mailModel);
        }

        //
        // GET: /Mail/Compose

        public ActionResult Compose()
        {
            return View();
        }

        //
        // POST: /Mail/Compose

        [HttpPost]
        public ActionResult Compose(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
