﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MailDomain;

namespace MailClient.Controllers.Helper
{
    public interface IUserMailHelper
    {
        UserProfile GetUser(string userName);
        string GetMailBox(string inputMailBox);
    }
}