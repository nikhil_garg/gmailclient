﻿using MailDomain;
using MailPersistence.Context;
using MailPersistence.UnitOfWork;
using System.Linq;

namespace MailClient.Controllers.Helper
{
    public class UserMailHelper : IUserMailHelper
    {
        public UserProfile GetUser(string userName)
        {
            UserProfile user;
            using (var uow = new UnitOfWork(new MailClientContext()))
            {
                user = uow.UserProfileRepository.Get(u => u.UserName == userName).First();
            }
            return user;
        }

        public string GetMailBox(string inputMailBox)
        {
            var mailBox = inputMailBox;
            if (inputMailBox.ToLower() == "sent")
                mailBox = "[Gmail]/Sent Mail";
            return mailBox;
        }
    }
}