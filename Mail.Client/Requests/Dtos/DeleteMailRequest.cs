﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MailClient.Requests.Dtos
{
    public class DeleteMailRequest
    {
        public IEnumerable<string> Ids { get; set; } 
    }
}