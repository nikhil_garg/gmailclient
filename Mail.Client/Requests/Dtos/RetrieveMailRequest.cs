﻿
namespace MailClient.Requests.Dtos
{
    public class RetrieveMailRequest
    {
        public string Username { get; set; }
        public string MailBox { get; set; }
        public int StartIndex { get; set; }
        public int EndIndex { get; set; }
        public int Length { get; set; }
    }
}