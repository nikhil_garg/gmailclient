﻿using System.Collections.Generic;
using MailClient.Dtos;

namespace MailClient.Requests.Dtos
{
    public class SendMailRequest
    {
        public string Subject { get; set; }
        public string Body { get; set; }
        public List<MailAddress> To { get; set; }
        public List<MailAddress> Cc { get; set; }
        public List<MailAddress> Bcc { get; set; }
        public List<MailAddress> ReplyTo { get; set; }
        public List<MailAttachment> Attachments { get; set; }
    }
}