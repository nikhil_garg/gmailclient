﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MailClient.Requests.Dtos
{
    public class MailDetailsRequest
    {
        public string Username { get; set; }
        public string MessageId { get; set; }
        public string MailBox { get; set; }
    }
}