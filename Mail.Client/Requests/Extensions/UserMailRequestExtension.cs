﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using DotNetOpenAuth.Messaging;
using MailClient.Requests.Dtos;
using MailDomain.Configurations.Imap;
using MailDomain.Configurations.Smtp;
using MailDomain.MailReceiver.Dto;
using MailDomain.MailRemover.Dto;
using MailDomain.MailSender.Dto;

namespace MailClient.Requests.Extensions
{
    public static class UserMailRequestExtension
    {
        public static RetrieveMailRequest GetRetrieveMailRequest(string userName, string mailBox, int startIndex = 0, int endIndex = 0, int length = 0)
        {
            return new RetrieveMailRequest { Username = userName, MailBox = mailBox, StartIndex = startIndex, EndIndex = endIndex, Length = length };
        }

        public static MailReceiverDto ToMailReceiverDto(this RetrieveMailRequest retrieveMailRequest, string password,
                                                                bool requireDetails, IImapConfiguration imapConfiguration)
        {
            imapConfiguration.Username = retrieveMailRequest.Username;
            imapConfiguration.Password = password;

            return new MailReceiverDto
            {
                StartIndex = retrieveMailRequest.StartIndex,
                EndIndex = retrieveMailRequest.EndIndex,
                Length = retrieveMailRequest.Length,
                MailBox = retrieveMailRequest.MailBox,
                RequireDetails = requireDetails,
                Imap = imapConfiguration
            };
        }

        public static MailDetailsRequest GetMailDetailsRequest(string userName, string mailBox, string messageId)
        {
            return new MailDetailsRequest { Username = userName, MessageId = messageId, MailBox = mailBox };
        }

        public static MailReceiverDto ToMailDetailsDto(this MailDetailsRequest mailDetailsRequest, string password,
                                                                bool requireDetails, IImapConfiguration imapConfiguration)
        {
            imapConfiguration.Username = mailDetailsRequest.Username;
            imapConfiguration.Password = password;

            return new MailReceiverDto
            {
                Id = mailDetailsRequest.MessageId,
                MailBox = mailDetailsRequest.MailBox,
                RequireDetails = requireDetails,
                Imap = imapConfiguration
            };
        }

        public static MailRemoverDto ToMailRemoverDto(this DeleteMailRequest deleteMailRequest, string userName, string password, 
                                                          string mailBox, IImapConfiguration imapConfiguration)
        {
            imapConfiguration.Username = userName;
            imapConfiguration.Password = password;

            return new MailRemoverDto
            {
                MailBox = mailBox,
                Ids = deleteMailRequest.Ids,
                Imap = imapConfiguration
            };
        }

        public static MailSenderDto ToMailSenderDto(this SendMailRequest sendMailRequest, string userName, string password, ISmtpConfiguration smtpConfiguration)
        {
            smtpConfiguration.Username = userName;
            smtpConfiguration.Password = password;

            var mailMessage = new MailMessage
            {
                Subject = sendMailRequest.Subject,
                Body = sendMailRequest.Body,
                From = new MailAddress(userName)
            };
            mailMessage.To.AddRange(sendMailRequest.To.ToMailAddressCollection());
            mailMessage.ReplyToList.AddRange(sendMailRequest.ReplyTo.ToMailAddressCollection());
            mailMessage.CC.AddRange(sendMailRequest.Cc.ToMailAddressCollection());
            mailMessage.Bcc.AddRange(sendMailRequest.Bcc.ToMailAddressCollection());

            return new MailSenderDto
            {
                Smtp = smtpConfiguration,
                MailMessage = mailMessage
            };
        }

        private static IEnumerable<MailAddress> ToMailAddressCollection(this IEnumerable<MailClient.Dtos.MailAddress> customMailAddresses)
        {
            return
                customMailAddresses.Select(
                    customMailAddress => new MailAddress(customMailAddress.Address, null))
                    .ToList();
        }
    }
}