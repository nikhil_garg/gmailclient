﻿using System;

namespace MailClient.Models
{
    public class MailModel
    {
        public int MailId { get; set; }

        public string MailBox { get; set; }

        public string To { get; set; }

        public string ReplyTo { get; set; }

        public string From { get; set; }

        public string CC { get; set; }

        public string BCC { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }
    }
}