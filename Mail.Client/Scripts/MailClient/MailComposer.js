﻿$(document).ready(function () {
    
    var getSendMailUrl = function () {
        return BASE_HREF + '/api/Users/' + $('#User_Identity_Name').val() + '/Mail/Send';
    };

    $('#sendMail').click(function() {
        console.log('send mail clicked');

        $.ajax({
            url: getSendMailUrl(),
            type: "POST",
            data: JSON.stringify({
                "Subject": $('#Subject').val(),
                "To": [{
                    "Address": $('#To').val()
                }],
                "Cc": [{
                    "Address": $('#CC').val()
                }],
                "Bcc": [{
                    "Address": $('#BCC').val()
                }],
                "ReplyTo": [{
                    "Address": $('#ReplyTo').val()
                }],
                "Body": $('#Body').val()
            }),
            contentType: "application/json; charset=utf-8",
            success: function (data, status, xhr) {
                console.log("Mail send successfully!");

                window.location.href = "/Mail/Index";
            },
            error: function (xhr, status, error) {
                console.log(error);
            }
        });
    });
});