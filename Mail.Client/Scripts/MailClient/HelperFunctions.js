﻿var BASE_HREF = document.location.origin;

var concatEmailsFull = function(emailList) {
    var emails = [];

    for (var item of emailList) {

        var email;

        if (item.DisplayName) {
            email = item.DisplayName + " <" + item.Address + ">";
        } else {
            email = item.Address;
        }

        emails.push(email);
    }

    return emails.join("; ");
};

var concatEmailsShort = function(emailList) {
    var emails = [];

    for (var item of emailList) {

        var email;

        if (item.DisplayName) {
            email = item.DisplayName;
        } else {
            email = item.Address;
        }

        emails.push(email);
    }

    return emails.join("; ");
};

