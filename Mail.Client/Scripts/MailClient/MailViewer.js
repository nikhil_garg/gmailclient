﻿var gridData;

$(document).ready(function () {
    
    var mailViewerGrid;

    var getMailUrl = function() {
        return BASE_HREF + '/api/Users/' + $('#User_Identity_Name').val() +
            '/' + $('#ViewerPage').val() + '/';
    };
    
    var bindMailViewerGrid = function () {
        
        mailViewerGrid = $('#mailViewerGrid').DataTable({
            "processing": true,
            "serverSide": true,
            
            ajax: {
                "url": getMailUrl() + 'Mail',
                "type": "GET"
            },
            
            pagingType: "simple_numbers",
            
            columns: [
                {
                    title: "",
                    data: "IsSelected",
                    render: function (isSelected, type, row) {
                        if (type === 'display') {
                            return '<input type="checkbox" class="editor-active">';
                        }
                        return isSelected;
                    }
                },
                {
                    title: $('#ViewerPage').val() === 'Inbox' ? "From" : "To",
                    data: $('#ViewerPage').val() === 'Inbox' ? "From" : "To",
                    render: function (data) {
                        var address = $('#ViewerPage').val() === 'Inbox' ? new Array(data) : data;
                        
                        return concatEmailsShort(address);
                    }
                },
                {
                    title: "Subject",
                    data: "Subject"
                },
                {
                    title: "Date",
                    data: "Date",
                    render: function (date) {
                        return moment(date).format('MMM DD YYYY h:mm A');
                    }
                }
            ],

            bDestroy: true
        });

        $('#loadingImage').hide();
    };
    
    $('#mailViewerGrid').on('page.dt', function () {
        var info = mailViewerGrid.page.info();
        $('#pageInfo').html('Showing page: ' + (info.page + 1) + ' of ' + info.pages);
    });

    $('#loadingImage').show();  

    bindMailViewerGrid();
    
    $('.sorting_1').click(function() {
        console.log('check box clicked');
    });
    
    $('#deleteMails').click(function () {
        console.log('delete mails clicked');

        var mailsToDelete = _.select(gridData, function(data) {
            return data.IsSelected === 'checked';
        });
        
        var mailIdsToDelete = _.map(mailsToDelete, function(mail) {
            return mail.Id;
        });

        if (mailIdsToDelete && mailIdsToDelete.length > 0) {
            $.ajax({
                url: getMailUrl() + 'Delete',
                type: "DELETE",
                data: JSON.stringify({
                    "Ids": mailIdsToDelete
                }),
                contentType: "application/json; charset=utf-8",
                success: function(data, status, xhr) {
                    console.log(mailIdsToDelete);
                    console.log(" mails deleted!");

                    window.location.href =
                        $('#ViewerPage').val() === 'Inbox' ? "/Mail/Index" : "/Mail/SentMail";
                },
                error: function(xhr, status, error) {
                    console.log(error);
                }
            });
        }
    });
    
    $('#mailViewerGrid').on('click', 'td', function (event) {
        
        var mailId = mailViewerGrid.row(this).data().Id;

        var column = $(this).attr('class');
        
        console.log(column);

        if (column === 'sorting_1') {
            var checkboxElem = $(this).find('input:checkbox:first');

            var isChecked = checkboxElem.attr('checked');

            mailViewerGrid.row(this).data().IsSelected = isChecked;

            gridData = mailViewerGrid.data();

        } else {
            window.location.href = "/Mail/Details?mailBox=" + $('#ViewerPage').val() + "&id=" + mailId;
        }
    });
});