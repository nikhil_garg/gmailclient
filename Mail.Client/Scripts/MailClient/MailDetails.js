﻿$(document).ready(function () {

    var id = $('#MailId').val();

    var getMailDetails = function(data) {
        var email = data;

        $('#To').text(concatEmailsFull(email.To));

        $('#From').text(concatEmailsFull(new Array(email.From)));

        $('#CC').text(concatEmailsFull(email.Cc));

        $('#BCC').text(concatEmailsFull(email.Bcc));

        $('#Subject').text(email.Subject);

        $('#Body').html(email.Body);

        $('#loadingImage').hide();
    };

    $('#loadingImage').show();
    
    // http://localhost:64091/api/Users/xyz@gmail.com/Inbox/Mail/52

    $.getJSON(BASE_HREF + '/api/Users/' + $('#User_Identity_Name').val() +
        '/' + $('#MailBox').val() + '/Mail/' + id, null, getMailDetails);
});