﻿using System;
using System.Collections.Generic;
using MailClient.Dtos;

namespace MailClient.Responses.Dtos
{
    public class MailDetails
    {
        public string Id { get; set; }
        public DateTime Date { get; set; }
        public bool Seen { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public MailAddress From { get; set; }
        public List<MailAddress> To { get; set; }
        public List<MailAddress> Cc { get; set; }
        public List<MailAddress> Bcc { get; set; }
        public List<MailAddress> ReplyTo { get; set; }
        public List<MailAttachment> Attachments { get; set; }
        public bool Important { get; set; }
        public bool IsSelected { get; set; }
    }
}