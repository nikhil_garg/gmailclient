﻿using System.Collections.Generic;

namespace MailClient.Responses.Dtos
{
    public class MailMessageResponse
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<MailDetails> data { get; set; }
    }
}