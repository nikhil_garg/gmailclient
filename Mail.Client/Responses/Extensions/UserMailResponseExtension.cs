﻿using System.Linq;
using MailClient.Responses.Dtos;
using System.Collections.Generic;
using System.Net.Mail;
using MailDomain.Dtos;
using MailAddress = MailClient.Dtos.MailAddress;

namespace MailClient.Responses.Extensions
{
    public static class UserMailResponseExtension
    {
        public static MailMessageResponse ToMailMessageResponseList(this IEnumerable<MailMessageDto> mailMessages)
        {
            return new MailMessageResponse
            {
                draw = 0,
                recordsTotal = mailMessages.Any() ? mailMessages.First().Count : 0,
                recordsFiltered = mailMessages.Any() ? mailMessages.First().Count : 0,
                data = mailMessages.Select(ToMailMessageResponse).Reverse().ToList()
            };
        }

        public static MailDetails ToMailMessageResponse(this MailMessageDto mailMessageDto)
        {
            return new MailDetails
            {
                Id = mailMessageDto.Id,
                Date = mailMessageDto.Date,
                From = mailMessageDto.From.ToCustomMailAddress(),
                To = mailMessageDto.To.ToCustomMailAddresses(),
                Cc = mailMessageDto.CC.ToCustomMailAddresses(),
                Bcc = mailMessageDto.Bcc.ToCustomMailAddresses(),
                ReplyTo = mailMessageDto.ReplyToList.ToCustomMailAddresses(),
                Seen = mailMessageDto.Seen,
                Important = mailMessageDto.Important,
                Subject = mailMessageDto.Subject,
                Body = mailMessageDto.Body
            };
        }

        private static List<MailAddress> ToCustomMailAddresses(this MailAddressCollection mailAddressCollection)
        {
            return mailAddressCollection.Select(mailAddress => new MailAddress
            {
                DisplayName = mailAddress.DisplayName,
                Address = mailAddress.Address
            }).ToList();
        }

        private static MailAddress ToCustomMailAddress(this System.Net.Mail.MailAddress mailAddress)
        {
            var mailAddressCollection = new MailAddressCollection
            {
                mailAddress
            };

            return ToCustomMailAddresses(mailAddressCollection).FirstOrDefault();
        }
    }
}