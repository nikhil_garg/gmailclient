﻿namespace MailClient.Dtos
{
    public class MailAttachment
    {
        public string ContentType { get; set; }
        public string FileName { get; set; }
        public string Content { get; set; }
    }
}