﻿namespace MailClient.Dtos
{
    public class MailAddress
    {
        public string DisplayName { get; set; }
        public string Address { get; set; }
    }
}