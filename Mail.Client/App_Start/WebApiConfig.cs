﻿using System.Web.Http;

namespace MailClient.App_Start
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute(
                name: "MailDetailsApi",
                routeTemplate: "api/Users/{userName}/{mailBox}/Mail/{messageId}",
                defaults: new
                {
                    controller = "UserMail",
                    action = "Details"
                }
            );

            config.Routes.MapHttpRoute(
                name: "DeleteMailApi",
                routeTemplate: "api/Users/{userName}/{mailBox}/Delete/",
                defaults: new
                {
                    controller = "UserMail",
                    action = "Delete"
                }
            );

            config.Routes.MapHttpRoute(
                name: "SendMailApi",
                routeTemplate: "api/Users/{userName}/Mail/Send",
                defaults: new
                {
                    controller = "UserMail",
                    action = "Send"
                }
            );

            config.Routes.MapHttpRoute(
                name: "UserMailApi",
                routeTemplate: "api/Users/{userName}/{mailBox}/Mail",
                defaults: new
                {
                    controller = "UserMail",
                    action = "Retrieve"
                }
            );

            config.Routes.MapHttpRoute(
                name: "UserCountApi",
                routeTemplate: "api/Users/{userName}/{mailBox}/Count",
                defaults: new
                {
                    controller = "UserMail",
                    action = "Count"
                }
            );

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            // Uncomment the following line of code to enable query support for actions with an IQueryable or IQueryable<T> return type.
            // To avoid processing unexpected or malicious queries, use the validation settings on QueryableAttribute to validate incoming queries.
            // For more information, visit http://go.microsoft.com/fwlink/?LinkId=279712.
            //config.EnableQuerySupport();
        }
    }
}