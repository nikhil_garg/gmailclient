﻿using MailClient.Models;
using System.Data.Entity.Migrations;
using WebMatrix.WebData;

namespace MailClient.App_Start
{
    public class MigrationConfig : DbMigrationsConfiguration<UsersContext> 
    {
        public MigrationConfig()
        {
            this.AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(UsersContext context)
        {
            WebSecurity.InitializeDatabaseConnection("DefaultConnection", "UserProfile", "UserId", "UserName", autoCreateTables: true);
        }
    }
}