﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using MailDomain;
using MailPersistence.Context;

namespace MailPersistence.Repositories
{
    public class UserProfileRepository : IRepository<UserProfile>
    {
        private readonly MailClientDataContext _context;

        public UserProfileRepository(IMailClientContext context)
        {
            _context = context as MailClientDataContext;
        }

        public IEnumerable<UserProfile> GetAll()
        {
            return _context.UserProfiles;
        }

        public IEnumerable<UserProfile> Get(Func<UserProfile, bool> predicate)
        {
            return _context.UserProfiles.Where(predicate);
        }

        public void Add(UserProfile obj)
        {
            _context.UserProfiles.InsertOnSubmit(obj);
        }

        public bool Delete(UserProfile obj)
        {
            _context.UserProfiles.DeleteOnSubmit(obj);
            return true;
        }

        public bool Delete(int id)
        {
            var user = Get(u => u.UserId == id).First();
            _context.UserProfiles.DeleteOnSubmit(user);
            return true;
        }
    }
}
