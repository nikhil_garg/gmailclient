﻿using System;
using MailDomain;

namespace MailPersistence.UnitOfWork
{
    public interface IUnitOfWork
    {
        IRepository<UserProfile> UserProfileRepository { get; }
        void CommitUnitOfWork();
    }
}
