﻿using System;
using MailDomain;
using MailPersistence.Context;
using MailPersistence.Repositories;

namespace MailPersistence.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private readonly MailClientDataContext _context;
        private bool _disposed;
        private readonly Lazy<IRepository<UserProfile>> _userProfileRepository;

        public IRepository<UserProfile> UserProfileRepository
        {
            get
            {
                return _userProfileRepository.Value;
            }
        }

        public UnitOfWork(IMailClientContext context)
        {
            _context = context as MailClientDataContext;
            _userProfileRepository = new Lazy<IRepository<UserProfile>>(() => new UserProfileRepository(context));
        }

        public void CommitUnitOfWork()
        {
            _context.SubmitChanges();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool isDisposing)
        {
            if (!_disposed)
            {
                if (isDisposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }
    }
}
