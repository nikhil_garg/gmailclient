CREATE TABLE [dbo].[Users]
(
[UserKey] [uniqueidentifier] NOT NULL CONSTRAINT [DF__Users__UserKey__251C81ED] DEFAULT (newid()),
[FirstName] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LastName] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Password] [varbinary] (max) NOT NULL,
[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF__Users__CreatedOn__17036CC0] DEFAULT GETDATE(),
[ModifiedOn] [datetime] NOT NULL CONSTRAINT [DF__Users__ModifiedOn__17036CC0] DEFAULT GETDATE(),
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Users] ADD CONSTRAINT [PK__Users__296ADCF13E1D39E1] PRIMARY KEY CLUSTERED  ([UserKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Users] ADD CONSTRAINT [UQ_Users_Username_262C91FE] UNIQUE NONCLUSTERED  ([Email]) ON [PRIMARY]
GO